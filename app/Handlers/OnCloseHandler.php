<?php
namespace App\Handlers;

use App\Helpers\Logger;
use Workerman\Connection\TcpConnection;

class OnCloseHandler extends Handler {

  public static function handler(TcpConnection $connection) {
    Logger::info("Client Disconnected.... ",$connection->id);
  }

}