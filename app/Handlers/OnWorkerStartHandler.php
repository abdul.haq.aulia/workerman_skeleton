<?php
namespace App\Handlers;

use App\Helpers\Logger;
use App\Helpers\RedisHelper;
use Workerman\Worker;

class OnWorkerStartHandler extends Handler {
  public static function handler(Worker $worker) {
    Logger::info($_ENV['APP_NAME'] ." [".$_ENV['APP_ENV']."], HTTP-Worker Start [".$_ENV['APP_URL']."] ");
    new RedisHelper();
  }

}