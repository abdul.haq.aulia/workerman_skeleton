<?php
namespace App\Handlers;

use App\Helpers\Logger;
use Workerman\Connection\TcpConnection;

class OnConnectHandler extends Handler {

  public static function handler(TcpConnection $connection) {
    Logger::info("Client-Connected.... ",$connection->id);
  }

}