<?php

namespace App\Handlers;

use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Response;

class Handler
{
    public static function responseJson(TcpConnection $connection, array $data, string $message = 'success', int $status = 200)
    {
        $response = new Response(
            $status,
            [
                'Content-type' => 'application/json'
            ],
            json_encode($data)
        );
        return $connection->send($response);
    }
}
