<?php

namespace App\Handlers;

use Andromeda\ISO8583\Parser;
use App\Definer\MessageDefiner;
use App\Helpers\HttpResponseHelper;
use App\Helpers\Logger;
use App\Helpers\RequestValidation;
use App\Helpers\TcpResponseHelper;
use App\Request\FinancialRequest;
use App\UseCases\Tcp\NetworkManagementUseCase;
use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Request;

class OnMessageHandler extends Handler
{
    public static function handler(TcpConnection $connection, Request | string $request)
    {
        /**
         * Example for Handle iso tcp
         */
        if (is_string($request)) {
            try {


                // Validasi panjang data
                $dataLengthAsciiRaw = substr($request, 0, 2);
                $dataMessage = "";
                $dataLength = "";
                $dataLengthAscii = "";

                if (strlen($dataLengthAsciiRaw) > 0) {
                    Logger::info("Data 1 ", $dataLengthAsciiRaw[0]);
                    Logger::info("Data 2 ", $dataLengthAsciiRaw[1]);
                    $headerDatas = str_split($dataLengthAsciiRaw);
                    $panjangData = ord($headerDatas[1]) + ord($headerDatas[1]) * 256;
                    Logger::info("Data Length => ", $panjangData);
                    Logger::info("Data Length => ", ord($dataLengthAsciiRaw[1]) + ord($dataLengthAsciiRaw[1]));
                    $dataLength = ord($dataLengthAscii);
                    $dataMessage = substr($request, 2, strlen($request));
                } else {
                    $dataMessage = $request;
                }

                $dataMessage = substr($request, 2, strlen($request));

                Logger::info("Incomming Message", [
                    "connection" => $connection->id,
                    "ascii_data_length" => $dataLengthAscii . "(" . strlen($dataLengthAscii) . ")",
                    "data_length" => $dataLength,
                    "iso_raw_message" => $request,
                    "iso_message" => $dataMessage . "(" . strlen($dataMessage) . ")",
                ]);

                Logger::info("Check Panjang Data => ", [
                    "dataLength" => (int)$dataLength,
                    "messageLength" => strlen($dataMessage)
                ]);

                // if ((int)$dataLength != strlen($dataMessage)) {
                //     $messageDefinerValidation = new SoppMessageDefiner();
                //     $parserValidation = new Parser($messageDefinerValidation);
                //     $parserValidation->addMessage($dataMessage);
                //     $mtiResponse = $parserValidation->getMTI() == "0200" ? "0210" : "0810";
                //     $code = "98";
                //     return TcpResponseHelper::generateValidationReponse($connection, $parserValidation, ResponseCodeBillerTranslatorHelper::get_message($code), $code, $mtiResponse, ResponseCodeBillerTranslatorHelper::get_message($code));
                // }

                $messageDefiner = new MessageDefiner();
                $parser = new Parser($messageDefiner);
                // Parsin Iso To Object
                $parser->addMessage($dataMessage);

                $rawData = new FinancialRequest($parser);

                $data = $rawData->getData();
                Logger::info("processing_code", $data->processing_code);


                if (in_array($parser->getMTI(), ["0800"])) { // type network management
                    if ($parser->getBit(70) == "001") { // sign on
                        return NetworkManagementUseCase::signOn($connection, $data, $parser, $request);
                    }

                    if ($parser->getBit(70) == "002") { // sign off
                        return NetworkManagementUseCase::signOff($connection, $data, $parser, $request);
                    }

                    if ($parser->getBit(70) == "301") { // echo Test
                        return NetworkManagementUseCase::echoTest($connection, $data, $parser, $request);
                    }
                } elseif (in_array($parser->getMTI(), ["0200"])) { // type financial transaction
                    
                } elseif (in_array($parser->getMTI(), ["0220", "0221", "0500"])) { // type financial transaction
                    if (in_array($data->processing_code, [380001, 500100])) { // Advice
                        
                    }
                }


                return TcpResponseHelper::generateResponse($connection, $parser, TcpResponseHelper::generateData((object)[]), "99", "Unknown Processing Code");
            } catch (\Throwable $th) {
                Logger::error("ERROR => ", $th->getMessage());
                Logger::error("ERROR TRACE => ", $th->getTrace());
                return TcpResponseHelper::generateResponse($connection, $parser, TcpResponseHelper::generateData((object)[]), "99", $th->getMessage());
            }
        } else {
            try {
                // Set action with switch case
                switch ($request->path()) {
                    /**
                     * Additional Payload @Json
                     * @param string sign
                     */
                    case '/api': // i
                        /** 
                         * Validate incomming method
                         */
                        RequestValidation::validateMehtod(["POST"], $request);
                        return NetworkManagementUseCase::welcome($connection, $request);
                        break;
    
                        /**
                         * Additional Payload @Json
                         * @param string sign
                         */
                    default:
                        return HttpResponseHelper::generateResponse($connection, null, "Unnkown route " . $request->path(), 404);
                        break;
                }
            } catch (\Throwable $th) {
                return HttpResponseHelper::generateResponse($connection, null, "99", $th->getMessage() . $request->path(), 500);
            }
        }
    }

}
