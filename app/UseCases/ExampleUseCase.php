<?php
namespace App\UseCases;

use Andromeda\ISO8583\Parser;
use App\Helpers\ResponseCodeMapHelper;
use App\Helpers\TcpResponseHelper;
use Workerman\Connection\TcpConnection;

class ExampleUseCase extends UseCase
{
  public static function index(TcpConnection $connection, object $data, Parser $parser, string $clientMessage = null) {
    return TcpResponseHelper::generateResponse($connection, $parser, "INI CONTOH MESSAGE SRING", "00", null, ResponseCodeMapHelper::get_message("00"));
  }
}
