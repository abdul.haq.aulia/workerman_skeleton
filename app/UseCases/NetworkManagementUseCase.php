<?php
namespace App\UseCases\Tcp;

use Andromeda\ISO8583\Parser;
use App\Helpers\HttpResponseHelper;
use App\Helpers\Logger;
use App\Helpers\ResponseCodeMapHelper;
use App\Helpers\ResponseCodeMpnTranslatorHelper;
use App\Helpers\TcpResponseHelper;
use App\UseCases\UseCase;
use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Request;

class NetworkManagementUseCase extends UseCase {

  public static function signOn(TcpConnection $connection, object $data, Parser $parser, string $clientMessage = null) {
    try {
      return TcpResponseHelper::generateNetworkManagementResponse($connection, $parser, "SUCCESS", "00", ResponseCodeMapHelper::get_message("00"));
    } catch (\Throwable $th) {
      Logger::error("ERROR", $th);
      return TcpResponseHelper::generateNetworkManagementResponse($connection, $parser, $th->getMessage(), "99", $th->getMessage());
    }
  }

  public static function signOff(TcpConnection $connection, object $data, Parser $parser, string $clientMessage = null) {
    try {
      return TcpResponseHelper::generateNetworkManagementResponse($connection, $parser, "SUCCESS", "00", ResponseCodeMapHelper::get_message("00"));
    } catch (\Throwable $th) {
      Logger::error("ERROR", $th);
    return TcpResponseHelper::generateNetworkManagementResponse($connection, $parser, $th->getMessage(), "99", $th->getMessage());
    }
  }

  public static function echoTest(TcpConnection $connection, object $data, Parser $parser, string $clientMessage = null) {
    try {
      return TcpResponseHelper::generateNetworkManagementResponse($connection, $parser, "SUCCESS", "00", ResponseCodeMapHelper::get_message("00"));
    } catch (\Throwable $th) {
      Logger::error("ERROR", $th);
      return TcpResponseHelper::generateNetworkManagementResponse($connection, $parser, $th->getMessage(), "99", $th->getMessage());
    }
  }

  public static function welcome(TcpConnection $connection, Request $request) { 
       return HttpResponseHelper::generateResponse($connection, [], "00",  "success", 200);
  }

}