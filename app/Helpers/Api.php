<?php
namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;


class Api {
  public static function fetch($method, $url, $payload, $bearerToken = null, $authToken = null) {
    $client = new Client();
    $unikID = uniqid('#');
    Logger::info("[". $unikID . "][HOST][REQUEST][".$url."][PAYLOAD]", json_encode($payload));
    try {
        $options = [
            'timeout' => $_ENV['TIMEOUT'],
            'headers' => [
                "Content-Type" => "application/json",
                "Accept" => "application/json"
            ]
        ];

        if ($bearerToken != null) {
            $options['headers']['Authorization'] = "$bearerToken";
        }

        if ($authToken != null) {
            $options['headers']['Authorization'] = "$authToken";
        }

        if ($method == "GET") {
            $options['query'] = $payload;
        }

        if ($method == "POST") {
            $options['body'] = json_encode($payload);
        }

        Logger::info("[". $unikID . "][HOST][REQUEST][".$url."]['HEADER']", json_encode($options));

        $response = $client->request($method, $url, $options);

        Logger::info("[". $unikID . "][HOST][REQUEST][".$url."] StatusCode => " . $response->getStatusCode());
        if ($response->getStatusCode() == 200) {     
            $responseBody = $response->getBody()->getContents();       
            Logger::info("[". $unikID . "][HOST][RESPONSE][".$url."] Response Success => ", $responseBody);
            return (object)[
                "status" => "000",
                "message" => "Success",
                "data" => json_decode($responseBody),
            ];
        } else {
            Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Response => ", $response->getBody());
            return (object)[
                "status" => "999",
                "message" => "Error-".$response->getStatusCode(),
                "data" => json_decode($response->getBody()->getContents())
            ];
        }
    } catch (ClientException $e) {        
        $responseBodyAsString = $e->getResponse()->getBody(true);
        $responseError = $e->getMessage();
        Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Error Message => ", $responseError);
        Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Body Content => ", $responseBodyAsString);
        return (object)[
            "status" => "999",
            "message" => "Client Exception " . $responseError,
            "data" => $responseBodyAsString
        ];
    } catch (RequestException $e) {        
        $responseBodyAsString = $e->getMessage();
        $responseError = $e->getMessage();
        Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Error Message Request Exception => ", $responseError);
        Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Body Content => ", $responseBodyAsString);
        return (object)[
            "status" => "991",
            "message" => "Request Exception " . $responseError,
            "data" => $responseBodyAsString
        ];
    } catch (ConnectException $e) {        
        $responseBodyAsString = $e->getMessage();
        $responseError = $e->getMessage();
        Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Error Message Connection Exception => ", $responseError);
        Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Body Content => ", $responseBodyAsString);
        return (object)[
            "status" => "992",
            "message" => "Connections Timeout " . $responseBodyAsString,
            "data" => $responseBodyAsString
        ];     
    } catch (\Throwable $e) {
        $responseBodyAsString = $e->getMessage();
        $responseError = $e->getMessage();
        Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Error Message Throwable Exception => ", $responseError);
        Logger::error("[". $unikID . "][HOST][RESPONSE][".$url."] Body Content => ", $responseBodyAsString);
        return (object)[
            "status" => "993",
            "message" => "Exception " . $responseBodyAsString,
            "data" => $responseBodyAsString
        ];
    }
  }
}