<?php

namespace App\Helpers;

use PDO;

class Database
{
    public static function connect($type)
    {
        /**
     * Sopp Config
     * [database_SOPP]
     */
        $db_host_1 = $_ENV['DB_HOST_1'];
        $db_name_1 = $_ENV['DB_NAME_1'];
        $db_user_1 = $_ENV['DB_USERNAME_1'];
        $db_password_1 = $_ENV['DB_PASSWORD_1'];

        /**
         * MC Config
         * [database_MC]
         */
        $db_host_2 = $_ENV['DB_HOST_2'];
        $db_name_2 = $_ENV['DB_NAME_2'];
        $db_user_2 = $_ENV['DB_USERNAME_2'];
        $db_password_2 = $_ENV['DB_PASSWORD_2'];

        /**
         * Deter
         */
        Logger::info("Trying to connect to database....");
        $success = false;
        try {
            $servername = $type == "SOPP" ? $db_host_1 : $db_host_2;
            $username = $type == "SOPP" ? $db_user_1 : $db_user_2;
            $password = $type == "SOPP" ? $db_password_1 : $db_password_2;
            $database = $type == "SOPP" ? $db_name_1 : $db_name_2;
            $port = "1433";

            // Establish a connection to the SQL Server database
            // jdbc:sqlserver://;serverName=10.60.64.26;databaseName=master
            $conn = new PDO("sqlsrv:Server=$servername;Database=$database;Encrypt=no", $username, $password);

            $success = true;
        } catch (\Throwable $th) {
            $success = false;
            $message = $th->getMessage();
        }

        if (!$success) {
            // Send email ke operation, kalo ada masalah koneksi ke database
            // use wordwrap() if lines are longer than 70 characters
            $msg = wordwrap($message, 70);
            Logger::error($msg);
            // send email
            mail($_ENV['DEVELOPER_EMAIL'], "Error Database Connection", $msg);
            return;
        }
        Logger::info("Sukses koneksi ke database...");
        return $conn;
    }
}
