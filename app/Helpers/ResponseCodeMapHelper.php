<?php

namespace App\Helpers;

class ResponseCodeMapHelper
{
    public const SUCCESS = '00';
    public const KODE_BILLING_TIDAK_TERDAFTAR = '01';
    public const KODE_BILLING_EXPIRED = '02';
    public const TOTAL_TAGIHAN_TIDAK_SAMA = '03';
    public const NO_REKENING_TIDAK_TERDAFTAR = '04';
    public const KODE_PARTNER_TIDAK_TERDAFTAR = '05';
    public const AUTHCODE_SALAH = '06';
    public const TAGIHAN_TELAH_TERBAYAR_DI_BANK_PARTNER_LAIN = '27';
    public const KODE_PARTNER_SALAH = '31';
    public const KODE_MATA_UANG_TIDAK_TERDAFTAR = '32';
    public const KODE_KANAL_PEMBAYARAN_TIDAK_DITEMUKAN = '33';
    public const TAGIHAN_TELAH_TERBAYAR = '88';
    public const TIMEOUT = '90';
    public const EXCEPTION_ERROR = '92';
    public const INQUIRY_REQUEST_TIDAK_DITEMUKAN = '96';
    public const CA_BELUM_TERDAFTAR_DI_SATU_ZONA_WAKTU_TERTENTU = '97';
    public const INVALID_LENGTH_DATA = '98';
    public const ENTITY_DATA_BILLING_SALAH = '99';

    /**
     * Response message mapping from core banking
     *
     * @var array
     */
    private static $messages = [
      self::SUCCESS => 'Transaksi Berhasil',
      self::KODE_BILLING_TIDAK_TERDAFTAR => 'Kode Billing Tidak Terdaftar',
      self::KODE_BILLING_EXPIRED => 'Kode Billing Expired',
      self::TOTAL_TAGIHAN_TIDAK_SAMA => 'Total Tagihan Tidak Sama',
      self::NO_REKENING_TIDAK_TERDAFTAR => 'No Rekening Tidak Terdaftar',
      self::KODE_PARTNER_TIDAK_TERDAFTAR => 'Kode Partner Tidak Terdaftar',
      self::AUTHCODE_SALAH => 'Auth Code Salah',
      self::TAGIHAN_TELAH_TERBAYAR_DI_BANK_PARTNER_LAIN => 'Tagihan Telah Terbayar Di Bank/Partner Lain',
      self::KODE_PARTNER_SALAH => 'Kode Partner Salah',
      self::KODE_MATA_UANG_TIDAK_TERDAFTAR => 'Kode Mata uang Tidak Terdaftar',
      self::TAGIHAN_TELAH_TERBAYAR => 'Tagihan Telah Terbayar',
      self::INVALID_LENGTH_DATA => 'Panjang Length Message Tidak Sama',
      self::ENTITY_DATA_BILLING_SALAH => 'Entity Data Billing Salah',
      self::KODE_KANAL_PEMBAYARAN_TIDAK_DITEMUKAN => "Kode Kanal Pembayaran Tidak Ditemukan",
      self::TIMEOUT => "Timeout",
      self::EXCEPTION_ERROR => 'Exception Error',
      self::INQUIRY_REQUEST_TIDAK_DITEMUKAN => 'Inquiry Request Tidak Ditemukan',
      self::CA_BELUM_TERDAFTAR_DI_SATU_ZONA_WAKTU_TERTENTU => 'CA Belum Terdaftar Di Satu Zona Waktu Tertentu',
    ];

    public static function get_message($code)
    {
        return self::$messages[$code];
    }
}
