<?php

namespace App\Helpers;

use Redis;
class RedisHelper {

  private $redis;

  public function __construct()
  {
    $this->redis = new Redis();
    $this->redis->connect($_ENV['REDIS_HOST'], $_ENV['REDIS_PORT']);
    $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP);
    Logger::info($this->redis->ping("Success connect to redis server " . $_ENV['REDIS_HOST'] . ":" . $_ENV['REDIS_PORT']));
    $this->redis->select($_ENV['REDIS_DB']);
  }

  public function redis() {
    return $this->redis;
  }
}