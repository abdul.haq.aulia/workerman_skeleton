<?php

namespace App\Helpers;

use Andromeda\ISO8583\Parser;
use Workerman\Connection\TcpConnection;

class TcpResponseHelper
{

    public static function generateNetworkManagementResponse(TcpConnection $connection, Parser $rawData, string $responseData, string|int $code = 00, string $message = 'success')
    {
        $rawData->addMTI("0810");
        $rawData->addBit(39, $code);
        // $rawData->addBit(48, $responseData);
        
        Logger::info("Response Message", [
            "connection" => $connection->id,
            "data_length" => self::calculateHeaderLength($rawData->getISO()),
            "data_length_ascii" => self::calculateHeaderLength($rawData->getISO()) ,
            "iso_message" => self::calculateHeaderLength($rawData->getISO()) .$rawData->getISO(),
            "bitmap" => $rawData->getBitmap(),
            "iso_parsed" => $rawData->getData(),
        ]);
        
        return $connection->send(self::calculateHeaderLength($rawData->getISO()) .$rawData->getISO());
    }

    public static function generateResponse(TcpConnection $connection, Parser $rawData, string $responseData, string|int $code = 00, string $amount = null, string $message = 'success')
    {

        $rawData->addMTI("0210");
        if ($code !== "00") {
            $rawData->removeData(4);
        } else {
            Logger::info("AMOUNT => ", $amount);
            Logger::info("AMOUNT => ", (int)$amount);
            if ($amount != null) {
                $rawData->addBit(4, (int)$amount);
                $rawData->addBit(42, (int)$amount);
            }
        }
        $rawData->addBit(2, $rawData->getBit(2));
        $rawData->addBit(32, $rawData->getBit(32));
        $rawData->addBit(39, $code);
        $rawData->addBit(47, $rawData->getBit(47));
        $rawData->addBit(48, $responseData);
        $rawData->addBit(49, $rawData->getBit(49));
        $rawData->addBit(63, $rawData->getBit(63));
        
        Logger::info("Response Message", [
            "connection" => $connection->id,
            "data_length" => strlen($rawData->getISO()),
            "data_length_ascii" => self::calculateHeaderLength($rawData->getISO()) ,
            "iso_message" => self::calculateHeaderLength($rawData->getISO()) .$rawData->getISO(),
            "bitmap" => $rawData->getBitmap(),
            "iso_parsed" => $rawData->getData(),
        ]);
        
        return self::calculateHeaderLength($rawData->getISO()) . $rawData->getISO();
    }

    public static function generateResponseAdvice(TcpConnection $connection, Parser $rawData, string $responseData, string|int $code = 00, string $amount = null, string $message = 'success')
    {

        $rawData->addMTI("0510");
        if ($code !== "00") {
            $rawData->removeData(4);
        } else {
            Logger::info("AMOUNT => ", $amount);
            Logger::info("AMOUNT => ", (int)$amount);
            if ($amount != null) {
                $rawData->addBit(4, (int)$amount);
                $rawData->addBit(42, (int)$amount);
            }
        }
        if ($amount != null) {
            $rawData->addBit(4, (int)$amount);
            $rawData->addBit(42, (int)$amount);
        }
        $rawData->addBit(2, $rawData->getBit(2));
        $rawData->addBit(32, $rawData->getBit(32));
        $rawData->addBit(39, $code);
        $rawData->addBit(47, $rawData->getBit(47));
        $rawData->addBit(48, $responseData);
        $rawData->addBit(49, $rawData->getBit(49));
        $rawData->addBit(63, $rawData->getBit(63));
        
        Logger::info("Response Message", [
            "connection" => $connection->id,
            "data_length" => strlen($rawData->getISO()),
            "data_length_ascii" => self::calculateHeaderLength($rawData->getISO()) ,
            "iso_message" => self::calculateHeaderLength($rawData->getISO()) .$rawData->getISO(),
            "bitmap" => $rawData->getBitmap(),
            "iso_parsed" => $rawData->getData(),
        ]);
        
        return self::calculateHeaderLength($rawData->getISO()) . $rawData->getISO();
    }

    public static function generateValidationReponse(TcpConnection $connection, Parser $rawData, string $responseData, string|int $code = 00, string $mti = "0810", string $message = 'success')
    {
        $rawData->addMTI($mti);
        $rawData->addBit(39, $code);
        // $rawData->addBit(48, $responseData);
        $lengthAscii = 
        Logger::info("Response Message", [
            "connection" => $connection->id,
            "data_length" => strlen($rawData->getISO()),
            "data_length_ascii" => self::calculateHeaderLength($rawData->getISO()) ,
            "iso_message" => self::calculateHeaderLength($rawData->getISO()) .$rawData->getISO(),
            "bitmap" => $rawData->getBitmap(),
            "iso_parsed" => $rawData->getData(),
        ]);
        
        return $connection->send(self::calculateHeaderLength($rawData->getISO()) .$rawData->getISO());
    }

    public static function generateResponseInquiry(object $data, Parser $parser) {

        $message = "";

        
        return $message;
    }

    public static function generateResponsePayment(object $data, Parser $parser) {

        $message = "";

        return $message;
    }
    
    public static function generateAdviceResponse(object $data, Parser $parser) {

        $message = "";

        return $message;
    }
    
    public static function generateData(object $data)
    {
        $valuesArray = [];
        foreach ($data as $property => $value) {
            $valuesArray[] = $value;
        }

        // Define your delimiter
        $delimiter = '~#';

        // Implode the values with the delimiter
        $implodedString = implode($delimiter, $valuesArray);
        return $implodedString;
    }

    public static function calculateHeaderLength(string $message) {
        $messageLength = strlen($message);

        if ($messageLength > 255) {
            $hasilmod = $messageLength % 256;
            $hasilBagi = intdiv(($messageLength - $hasilmod), 256);
            return chr($hasilBagi).chr($hasilmod);
        }

        return chr(0).chr($messageLength);
    }

}
