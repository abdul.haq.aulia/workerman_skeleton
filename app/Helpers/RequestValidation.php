<?php
namespace App\Helpers;

use Exception;
use Workerman\Protocols\Http\Request;

class RequestValidation {
  public static function validate(array $request) {
    return $request;
  }

  public static function validateSign(array $sign) {
    try {
      return true;
    } catch (\Throwable $th) {
      throw new Exception($th);
    }
  }

  public static function validateMehtod(array $method, Request $request) {
    try {
      if (!in_array($request->method(), $method)) {
        throw new Exception("Method not allowed");
      }
      return true;
    } catch (\Throwable $th) {
      throw new Exception($th);
    }
  }
}