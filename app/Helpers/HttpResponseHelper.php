<?php

namespace App\Helpers;

use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Response;

class HttpResponseHelper
{
    public static function generateResponse(TcpConnection $connection, array|null $data, string|int $code = null, $message = 'success', int $status = 200)
    {
        $response = new Response(
            $status,
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                "status" => $status,
                "code" => $code,
                "message" => $message,
                "data" => $data == null ? null : $data
            ])
            
        );
        
        return $connection->send($response);
    }
}
