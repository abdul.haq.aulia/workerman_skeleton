<?php

namespace App\Helpers;

class Logger
{
    public static function info(string $message, $var = '')
    {
        self::printLog('info', $message, $var);
    }

    public static function waring($message, $var = '')
    {
        self::printLog('waring', $message, $var);
    }

    public static function error($message, $var = '')
    {
        self::printLog('error', $message, $var);
    }

    public static function saveToRedis() {
        // Check apakah 
    }

    private static function printLog($type, $message, $var)
    {
        if (in_array($_ENV['APP_ENV'], ['dev', 'local']) == true) {
            if ($type == "info") {
                $warna = "\e[93m [INFO]";
            } elseif ($type == "warning") {
                $warna = "\e[95m [WARNING]";
            } elseif ($type == "error") {
                $warna = "\e[91m [ERROR]";
            } else {
                $warna = "\e[91m [DEFAULT]";
            }
            $message = "\e[97m".$message."\e[92m";
            echo PHP_EOL."\e[36m[".date("Y-m-d H:i:s.").gettimeofday()["usec"]."]".$warna."\e[39m ".$message;
            if ($var !== "") {
                echo "\n\r";
                print_r($var);
            }
        } else {
            echo PHP_EOL."[".date("Y-m-d H:i:s.").gettimeofday()["usec"]."] [".$type."] ".$message;
            echo "\n\r";
            print_r($var);
        }
    }
}
