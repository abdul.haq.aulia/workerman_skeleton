<?php
namespace App\Request;

use Andromeda\ISO8583\Parser;

class FinancialRequest {
  private $data;

  public function __construct(Parser $rawData)
  {
    $this->data = self::parseData($rawData);
  }

  public function getData() {
    return $this->data;
  }

  // use this to define request from sopp
  private function parseData(Parser $rawData) {
    return (object) [
      "primary_account_number" => $rawData->getBit(2),
      "processing_code" => $rawData->getBit(3),
      "transaction_amout" => $rawData->getBit(4),
      "transaction_amout_converted" => $rawData->getBit(49) == 360 ? (int)$rawData->getBit(4) : (float) ltrim((int)$rawData->getBit(4), '0') / 100,
      "transmission_date_and_time" => $rawData->getBit(7),
      "transmission_date_and_time_converted" => $this->convertToDate($rawData->getBit(7)),
      "system_trace_audit_number" => $rawData->getBit(11),
      "local_transaction_time" => $rawData->getBit(12),
      "local_transaction_date" => $rawData->getBit(13),
      "local_transaction_date_time" => $this->convertToDate($rawData->getBit(13).$rawData->getBit(12)),
      "settelement_date" => $rawData->getBit(15), // transaksi di bawah cut of => H+0 = bit15 - 1, diatas jam cut off = bit15 (transaksi normal) || transaksi jumat, sabtu, minggu, semua dibuat hari senin | tangal hari libur nasional, di hari kerja berikutnya |
      "merchant_types" => $rawData->getBit(18),
      "acquiring_institution_identification_code" => $rawData->getBit(32),
      "retrieval_reference_number" => $rawData->getBit(37),
      "card_acceptor_terminal_identification" => $rawData->getBit(42),
      "card_acceptor_terminal_identification_converted" => $rawData->getBit(49) == 360 ? (int)$rawData->getBit(42) : (float) ltrim((int)$rawData->getBit(42), '0') / 100,
      "card_acceptor_name" => $rawData->getBit(43),
      "info_1" => $rawData->getBit(48),
      "transaction_currency_code" => $rawData->getBit(49),
      "transaction_currency_code_id" => $rawData->getBit(49) == "000" ? "IDR" : ($rawData->getBit(49) == 360 ? 'IDR' : 'USD'),
      "info_2" => $rawData->getBit(63),
    ];
  }

  public function convertToDate($data) {
    $y = date('Y');
    $m = substr($data, 0,2);
    $d = substr($data, 2, 2);
    $h = substr($data, 4, 2);
    $i = substr($data, 6, 2);
    $s = substr($data, 8, 2);
    return "$y-$m-$d $h:$i:$s";
  }
}