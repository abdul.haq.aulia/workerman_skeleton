# Workerman Skeleton v.0.0.1

# Cara Pemakaian

 - Clone repositori ini
   `git clone https://gitlab.com/abdul.haq.aulia/workerman_skeleton.git` nama_project_anda
   atau
   `git clone git@gitlab.com:abdul.haq.aulia/workerman_skeleton.git` nama_project_anda

 - Jalankan perintah
 `composer install`
  
 - Copy .env.example menjadi .env, lalu sesuikan konfigurasi aplikasi Anda
 - Jalankan aplikasi dengan cara mengetikan perintah
`php server start`  
`php server start -d`  

# Printah Lainnya
|No| Perintah | Keterangan |
|--|--|--|
|1  |php server start  | Menjalankan Server |
|2  |php server start -d  | Menjalankan Server Sebagai Daemon |
|3  |php server status  | Mengecek Status Server |
|4  |php server status -d  | Mengecek Status Sebagai Daemon |
|5  |php server connections  | Mengecek Koneksi |
|6  |php server stop  | Menghentikan Server |
|7  |php server stop -g  | Menghentikan Server Secara Global |
|8  |php server restart  | Merestart Server |
|9  |php server reload  | Mereload Server |
|10  |php server reload -g  | Mereload Server Secara Global |
